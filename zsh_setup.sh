# /bin/bash
SYS_LANG=$(locale | grep LANGUAGE)
if [[ $SYS_LANG == *"en_"* ]]; then
  echo "This script installs zsh and some other cool productive stuff.\nPay attention to it since it prompts for password inputs."
elif  [[ $SYS_LANG == *"pt_"* ]]; then
  echo "Este script instala o zsh e algumas outras coisas legais para produtividade.\nAtenção à execução, pois às vezes ele irá solicitar sua senha."
fi

git version > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Git OK"
else
  echo "Installing Git"
  sudo apt install -y git
fi

#installing zsh shell and optional dependencies
zsh --version > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "ZSH OK"
else
  echo "Installing ZSH"
  sudo apt install -y zsh
fi

#TODO: verify if things are already installed before trying
#installing powerline fonts for oh-my-zsh promptpower
sudo apt install -y powerline

#installing oh-my-zsh

sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

#installing custom bullet-train theme
wget -O ~/.oh-my-zsh/themes/bullet-train.zsh-theme https://bitbucket.org/edgar-anascimento/scripts/raw/a2997a704cd638ddbff8c6d39099f621916c5128/zsh_theme/bullet-train.zsh-theme

#TODO: install kubectl and helm